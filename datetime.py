import time
from datetime import datetime,timedelta
import datetime
from datetime import date
from datetime import   datetime, timedelta, time

#now function has the property of day, year and month 
current_time = datetime.datetime.now()

# Printing attributes of now().
print("The attributes of now() are : ")

print("Year : ", end="")
print(current_time.year)

print("Month : ", end="")
print(current_time.month)

print("Day : ", end="")
print(current_time.day)



#craete method for add the date


# Using current time
ini_time_for_now = datetime.now()

# printing initial_date
print("initial_date", str(ini_time_for_now))

# Calculating future dates
# for two years
future_date_after_2yrs = ini_time_for_now + \
    timedelta(days=730)

future_date_after_2days = ini_time_for_now + \
    timedelta(days=2)

# printing calculated future_dates
print('future_date_after_2yrs:', str(future_date_after_2yrs))
print('future_date_after_2days:', str(future_date_after_2days))
print("initial_date", str(ini_time_for_now))

# Some another datetime
new_final_time = ini_time_for_now + \
    timedelta(days=2)

# printing new final_date
print("new_final_time", str(new_final_time))


# printing calculated past_dates
print('Time difference:', str(new_final_time -
                              ini_time_for_now))



# initialize any date and time


x = datetime.datetime(2018, 5, 12, 2, 25, 50, 13)

print(x.strftime("%b %d %Y %H:%M:%S"))
  
# Initializing constructor  
# with time parameters as well 
    #   a = datetime(1999, 12, 12, 12, 12, 12, 60) 
    #   print(a)
       
